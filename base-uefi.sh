#!/bin/bash

ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
hwclock --systohc
sed -i '387s/.//' /etc/locale.gen
locale-gen
echo "LANG=pt_BR.UTF-8" >> /etc/locale.conf
echo "KEYMAP=br-abnt2" >> /etc/vconsole.conf
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts
echo root:1234 | chpasswd

pacman -S efibootmgr networkmanager network-manager-applet dialog wpa_supplicant base-devel linux-headers acpi acpi_call tlp acpid

# pacman -S --noconfirm xf86-video-amdgpu
pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

#grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB #change the directory to /boot/efi is you mounted the EFI partition at /boot/efi

#grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager

systemctl enable avahi-daemon
systemctl enable tlp # You can comment this command out if you didn't install tlp, see above
systemctl enable fstrim.timer
systemctl enable acpid


useradd -m luciano
echo luciano:1234 | chpasswd
usermod -aG wheel luciano

echo "luciano ALL=(ALL) ALL" >> /etc/sudoers.d/luciano

bootctl ----path=/boot install

echo "default  arc" >> /boot/loader/loader.conf


printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"




